# What it does

The script synchronises the content of a network directory (``-d /abs/path/to/dir``) to a specific folder of 
a Galaxy's Library (``-l galaxy_library_name`` and ``-f folder_name``). 
The network directory must be accessible from the server running galaxy.

# Notes:

- **This tool is only accessible to Galaxy admins**
- **All the files that are linked need to be readable by 'others' so the nginx process can serve the files**


# Installation and dependencies

* [bioblend](https://bioblend.readthedocs.io/en/latest/) is required and can be installed through conda:
```bash
# make sure to have the
conda create -n bioblend -c bioconda bioblend
```

* If you like, you can also install [parsec](https://parsec.readthedocs.io/en/latest/) for handling your 
galaxy credentials ``conda install parsec`` 
   * Then run ``parsec init`` to register your galaxy url and key and this tool will pick it up
* Alternatively, you can create the `~/.parsec.yml` file yourself. Add and adjust the following:
	```yaml
	__default: embl
    embl:
        key: "your-api-key"
        url: "https://galaxy.embl.de"
	```

# Details 

The directory sub-structure is also replicated in the Galaxy Library's folder and all 
files found in the directory sub-structure are added in the Galaxy's Library using symbolic links.
Compressed files with ``gz``, ``zip``, ``bz2`` extensions are properly handled. You can extend this list 
by adding missing extensions to the hard coded ``COMPRESSION_EXT_LIST`` list. In case you do this, 
make sure that your galaxy instance also properly handles file compressed with the new extensions. 

When ``-c`` is given (the default), the script first checks if all datasets listed in the galaxy 
library's folder (and sub-folders) still exist **in the synchronised directory** (i.e. given by ``-d``); 
and datasets with broken links are removed from the Galaxy library. 

By default all files and folders are synchronised excluding files which name starts with a dot ``.``. 
Additional files and/or folders can be ignored using a ``.galaxy_ignore`` file placed in the synchronized 
directory (i.e. directly in ``-d`` path). 
The ``.galaxy_ignore`` file expects one pattern per line, each pattern will be matched against 
the entire file or directory name (i.e. not the path, just the name). The wildcard char ``*`` can be used to 
mean *any character 0 or more times* while any other characters are escaped and interpreted as a normal 
character i.e. a ``.`` means *a dot* not *any letter*. 

For instance, all bam indices can be skipped by adding a ``*.bai`` expression in the ``.galaxy_ignore`` 
  
  
# Usage

## Example

```
python src/galaxy-dir-sync.py -u https://galaxy.embl.de -l TestData -f testsync -d /g/funcgen/tmp/galaxy/sync/ -k apikeyrandomcharacters -c --daemon -s 30
```

## Arguments

* ``--help``: show this help

The script takes the following mandatory arguments: 
* ``-d``: Absolute path to directory to sync with galaxy
* ``-l``: Name of the Galaxy Library. It should be already existing and you should have write permissions on it.
* ``-f``: Name of the folder to be used in the specified Galaxy Library. Will be created if not existing.

Mandatory if you haven't initialized parsec (or you want to connect to another galaxy instance)
* ``-u``: URL of the galaxy instance ; default to the value you gave at parsec init
* ``-k``: Your API key (to be first created in galaxy under the ``User > Preferences > Manage API key `` menu); 
default to the value you gave at parsec init

Optional :
* ``-c``: check for broken links (files registered in galaxy but files not found on disk)
* ``--daemon`` and ``-s``: run in daemon mode with a rest time of ``-s`` seconds between each round
* ``-v``: verbose level, see help
* ``--dryrun``: don't run for real, just prints what the script would do

# Caveats

- Can only be run by admins
- If files are not write protected users may delete/move or modify the files that are linked in Galaxy
and are used in tools. This breaks reproducibility!
